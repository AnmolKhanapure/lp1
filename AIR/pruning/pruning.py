#alpha best value that maximizer currently can gaurantee at that level of node
#beta best vlaue that the minimizer currently can gaurantee at the level of node

MAX,MIN=10000,-10000
def minmax(depth,nodeIndex,maximizingPlayer,values,alpha,beta):
    if depth == 3:
        return values[nodeIndex]
    
    if maximizingPlayer:
            best=MIN
            for i in range(0,2):
                val = minmax(depth+1,nodeIndex*2+i,False,values,alpha,beta)
                best = max(best,val)
                alpha = max(alpha,best)

                #pruning
                if beta <= alpha:
                    break
                return best
    else:
        best = MAX

        #recur for left children and right children
        for i in range(0,2):
            val = minmax(depth+1,nodeIndex*2+i,True,values,alpha,beta)
            best=min(best,val)
            beta=min(beta,best)

            if beta <= alpha:
                break
            return best 

if __name__ == "__main__":
    values = [3,5,4,6,7,8,-1,9]
    print("the optimal value is: ",minmax(0,0,True,values,MIN,MAX))