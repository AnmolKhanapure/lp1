#include<iostream>
#include<cstdlib>
#include<stdexcept>
#include<time.h>
#include<math.h>
using namespace std;

int NUMBER_OF_ELEMENTS = 1<<5;
int SIZE = NUMBER_OF_ELEMENTS*sizeof(int);

__global__ void kernel_sum(int* A,int* B,int* C, int n);
void sum(int* A,int* B,int* C,int n);

void sum(int* A,int* B,int* C,int n)
{
    int threadsPerBlock,blocksPerGrid;
    if(n<512)
    {
        threadsPerBlock = n;
        blocksPerGrid = 1;
    }
    else 
    {
        threadsPerBlock = n;
        blocksPerGrid = ceil(double(n)/double(threadsPerBlock));
    }
    kernel_sum<<<blocksPerGrid,threadsPerBlock>>>(A,B,C,n);
}
__global__ void kernel_sum(int* A,int* B,int* C,int n)
{
    int index = blockDim.x*blockIdx.x+threadIdx.x;
    if(index<n)
    {
        C[index]=A[index]+B[index];
    }
}

int main()
{
    int* hostA = (int*)malloc(SIZE);
    int* hostB = (int*)malloc(SIZE);
    int* hostC = (int*)malloc(SIZE);

    int *devA,*devB,*devC;
    srand(time(0));
    for(int i=0;i<NUMBER_OF_ELEMENTS;i++)
    {
        hostA[i]=rand()%20;
        hostB[i]=rand()%30;
    }

    cudaMalloc(&devA,SIZE);
    cudaMalloc(&devB,SIZE);
    cudaMalloc(&devC,SIZE);

    cudaMemcpy(devA,hostA,SIZE,cudaMemcpyHostToDevice);
    cudaMemcpy(devB,hostB,SIZE,cudaMemcpyHostToDevice);

    sum(devA,devB,devC,NUMBER_OF_ELEMENTS);

    cudaMemcpy(hostC,devC,SIZE,cudaMemcpyDeviceToHost);

    cudaFree(devA);
    cudaFree(devB);
    cudaFree(devC);

    double error = 0;
    for(int i=0;i<NUMBER_OF_ELEMENTS;i++)
    {
        double diff = double((hostA[i]+hostB[i])-hostC[i]);
        error+=diff;

        cout<<"A+B: "<<hostA[i]+hostB[i]<<endl;
        cout<<"C: "<<hostC[i]<<endl;
    }
    error = sqrt(error);
	cout<<"error  = "<<error<<endl;

	delete[] hostA;
    delete[] hostB;
    delete[] hostC;

    return cudaDeviceSynchronize();
}